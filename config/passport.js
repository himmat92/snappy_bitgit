var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

var User = require('../models/user');
var Merchant = require('../models/merchant');


passport.use('user', new LocalStrategy(
    function (username, password, done) {
        User.getUserByUsername(username, function (err, user) {
            if (err) throw err;
            if (!user) {
                return done(null, false, { message: 'Unknown User' });
            }

            User.comparePassword(password, user.password, function (err, isMatch) {
                if (err) throw err;
                if (isMatch) {
                    return done(null, user);
                } else {
                    return done(null, false, { message: 'Invalid password' });
                }
            });
        });
    }));

passport.use('merchant', new LocalStrategy(
    function (username, password, done) {
        Merchant.getUserByUsername(username, function (err, merchant) {
            if (err) throw err;
            if (!merchant) {
                return done(null, false, { message: 'Unknown User' });
            }

            Merchant.comparePassword(password, merchant.password, function (err, isMatch) {
                if (err) throw err;
                if (isMatch) {
                    return done(null, merchant);
                } else {
                    return done(null, false, { message: 'Invalid password' });
                }
            });
        });
    }));


passport.serializeUser(function (user, done) {
    User.findOne({username: user.username}, function (err, users) {
        if (users) {
            done(null, users.id);
        } 
        else{
         Merchant.findOne({username: user.username}, function (err, merchant) {
                if (merchant) {
                    done(null, merchant.id);
                }
            })
        }
    })
});


/*passport.serializeUser(function (user, done){
     if (User(user)) {
         done(null, user.id);
     } else if (Merchant(user)) {
        console.log(user);
            done(null, user.id);
     }
});*/


passport.deserializeUser(function (id, done) {
    User.getUserById(id, function (err, user) {
        if(err) done(err);
        if(user){
            done(null, user);
        } else {
            Merchant.getUserById(id, function (err, user) {
        if(err) done(err);
        done(null, user);
        })
    }
})
});


