var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var creqSchema = mongoose.Schema({
    username: {
        type: String,
        index:true
    },

    uusername: {
        type: String,
        index:true
    },

    email : {
        type: String,
    },

    amount: {
        type: Number,
        index:true
    },

    status: {
        type: String,
        default: "pending",

    }

});


var Card_req = module.exports = mongoose.model('Card_req', creqSchema);


module.exports.createUser = function(User, callback){
    User.save(callback);
};

module.exports.getUserByUsername = function(username, callback){
    var query = {username: username};
    Card_req.find(query, callback);


};

module.exports.getUserByuser = function(user, callback){
    var query = {user: user};
    Card_req.find(query, callback);

};

// This function is similar to the above one but this will only find the single user associated with that kind of data this function is used in login
module.exports.getUsersByUsername = function(username, callback){
    var query = {username: username};
    console.log(query);
    Card_req.findOne(query, callback);

};
// This will get the user by uniqid since we are using uniqid for issuing cards right now, this can be changed accordingly
module.exports.getUsersByUniqid = function(uniqid, callback){
    var query = {uniqid: uniqid};
    Card_req.findOne(query, callback);

};

module.exports.getUserById = function(id, callback){
    Card_req.findById(id, callback);

};

/*
module.exports.updateUserById = function(id, callback){
    var myquery = { _id: id };
    var newvalues = {$set: {redeem: "1"}, $set: {amount: "0"} };
    Issue.updateOne(myquery, newvalues, function(err, res) {
        if (err) throw err;
    })
};
*/
// Updating the database fields when a particular function is preformed, like making the amount nil when reedem button is clicked.
module.exports.updateUserById = function(id, callback){
    var myquery = { "_id": id };
    var newvalues = {$set: {redeem: "1", status: "issued"}};
    Card_req.update(myquery, newvalues);
    Card_req.findOne(myquery, callback);

};

// again performing actions on  a particular method, like changing the value after some amount is redeemed.
module.exports.updateUsersById = function(data, callback){
    var myquery = { "_id": data.id };
    var newvalues = {$set: {remaining_balance: data.rem_bal}};
    Card_req.update(myquery, newvalues, callback);

};




