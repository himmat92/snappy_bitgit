var mongoose = require('mongoose');

var issueSchema = mongoose.Schema({
    username: {
        type: String,
        index:true
    },

    uniqid : {
        type: String,
    },

    email: {
        type: String,
        index:true
    },

    amount: {
        type: Number,
        index:true
    },

    redeem: {
        type: Number,
        default: 0,

    },

    user: {
        type: String,
        index:true
    },

    remaining_balance: {
        type: Number,
        index: true
    },
});

// this function gives a name to the exported module to be called with this name, this modules is given name of Issue
var Issue = module.exports = mongoose.model('Issue', issueSchema);

// creating an function to create a new user with name createUser
module.exports.createUser = function(newUser, callback){
    // in the above line the data from the router is being pushed as newUser and is being called here and then saved in the next line
    // callback function works as soon as the newUser data is retrieved from the router.
    newUser.save(callback);
};
// The function will fetch the user from its username, this will be used when checking the issued cards for the user or other processes when necessary
module.exports.getUserByUsername = function(username, callback){
    // getting the username from the router from users line 213
    var query = {username: username};
    // In the above line the data is being querried with the model to find the desired data with that username
    Issue.find(query, callback);


    // In this it will find all the data related to the user and will push it to controller again

};

module.exports.getUserByuser = function(user, callback){
    var query = {user: user};
    Issue.find(query, callback);

};

// This function is similar to the above one but this will only find the single user associated with that kind of data this function is used in login
module.exports.getUsersByUsername = function(username, callback){
    var query = {username: username};
    Issue.findOne(query, callback);

};
// This will get the user by uniqid since we are using uniqid for issuing cards right now, this can be changed accordingly
module.exports.getUsersByUniqid = function(uniqid, callback){
    var query = {uniqid: uniqid};
    Issue.findOne(query, callback);
};

module.exports.getUserById = function(id, callback){
    Issue.findById(id, callback);

};

/*
module.exports.updateUserById = function(id, callback){
    var myquery = { _id: id };
    var newvalues = {$set: {redeem: "1"}, $set: {amount: "0"} };
    Issue.updateOne(myquery, newvalues, function(err, res) {
        if (err) throw err;
    })
};
*/
// Updating the database fields when a particular function is preformed, like making the amount nil when reedem button is clicked.
module.exports.updateUserById = function(id, callback){
    var myquery = { "_id": id };
    var newvalues = {$set: {redeem: "1", remaining_balance: "0"}};
    Issue.update(myquery, newvalues, callback);

};

module.exports.updateAountByUsername = function(user_data, callback){
    console.log(user_data);

    Issue.getUsersByUsername(user_data.username, function(err, users){
       console.log(users);
       query = {"username": users.username};
        var newvalues = {$set: {remaining_balance: users.amount+user_data.amount}};
        Issue.update(query, newvalues, callback);
    })

};

// again performing actions on  a particular method, like changing the value after some amount is redeemed.
module.exports.updateUsersById = function(data, callback){
    var myquery = { "_id": data.id };
    var newvalues = {$set: {remaining_balance: data.rem_bal}};
    Issue.update(myquery, newvalues, callback);

};

module.exports.updateUserByUsername = function(data, callback){
    var myquery = { "username": data.username };
    var newvalues = {}
}




