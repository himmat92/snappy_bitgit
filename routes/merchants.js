var express = require('express');
var router = express.Router();
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var uniqid = require('uniqid');
var objectId = require('mongodb').ObjectID;

var Merchant = require('../models/merchant');
var Issue = require('../models/issueCards');
var User = require('../models/user');
var Card_req = require('../models/cardrequest');


function ensureAuthenticated(req, res, next){
    if(req.isAuthenticated()){
        return next();
    } else {
        //req.flash('error_msg','You are not logged in');
        res.redirect('/users/login');
    }
}


// Register
router.get('/register', function (req, res) {
	res.render('merchant/register');
});

// Login
router.get('/login', function (req, res) {
	res.render('merchant/login');
});

router.get('/profile',ensureAuthenticated, function (req, res) {
    res.render('merchant/profile');
});

router.get('/issuecards', ensureAuthenticated, function(req, res){
    res.render('merchant/issuedCards')
});

// Register User
router.post('/register', function (req, res) {
	var name = req.body.name;
	var email = req.body.email;
	var username = req.body.username;
	var password = req.body.password;
	var password2 = req.body.password2;

	// Validation
	req.checkBody('name', 'Name is required').notEmpty();
	req.checkBody('email', 'Email is required').notEmpty();
	req.checkBody('email', 'Email is not valid').isEmail();
	req.checkBody('username', 'Username is required').notEmpty();
	req.checkBody('password', 'Password is required').notEmpty();
	req.checkBody('password2', 'Passwords do not match').equals(req.body.password);

	var errors = req.validationErrors();

	if (errors) {
		res.render('register', {
			errors: errors
		});
	}
	else {
		//checking for email and username are already taken
		Merchant.findOne({ username: {
			"$regex": "^" + username + "\\b", "$options": "i"
	}}, function (err, user) {
            Merchant.findOne({ email: {
				"$regex": "^" + email + "\\b", "$options": "i"
		}}, function (err, mail) {
				if (user || mail) {
					res.render('merchant/register', {
						user: user,
						mail: mail
					});
				}
				else {
					var newUser = new Merchant({
						name: name,
						email: email,
						username: username,
						password: password
					});
                    Merchant.createUser(newUser, function (err, user) {
						if (err) throw err;
					});
         	req.flash('success_msg', 'You are registered and can now login');
					res.redirect('/merchants/login');
				}
			});
		});
	}
});


router.post('/login',
	passport.authenticate('merchant', { successRedirect: '/merchants/profile', failureRedirect: '/merchants/login', failureFlash: true }),
	function (req, res) {
		res.redirect('/');
	});

router.get('/logout', function (req, res) {
	req.logout();

	req.flash('success_msg', 'You are logged out');

	res.redirect('/merchants/login');
});

router.post('/issuecard', function(req, res){
    const session_user = res.locals.user;
    const session_name = session_user.username;

    var username = req.body.name;
    var email = req.body.email;
    var amount = req.body.amount;

    //validations
    req.checkBody('name', 'Name is required').notEmpty();
    req.checkBody('email', 'Email is required').notEmpty();
    req.checkBody('email', 'Email is not valid').isEmail();
    req.checkBody('amount', 'Is required').notEmpty();

    var errors = req.validationErrors();


    if (errors) {
        res.render('merchant/login', {
            errors: errors

        });
    } else {
        User.findOne({ username:
                {"$regex": "^" + username + "\\b", "$options": "i"
                }}, function (err, user) {
            if (!user) {
                req.flash('error_msg','This user isnt registered');
                res.redirect('/merchants/profile');
            } else {
                var item = {

                    username: username,
                    uniqid: uniqid(),
                    email: email,
                    amount: amount,
                    user: session_name,
                    remaining_balance: amount,
                };
                var issueCard = new Issue(item);
                issueCard.save();

                res.redirect('profile');
            }
        });
    }
});

router.get('/get-data', function(req, res, next) {
    const session_user = res.locals.user;
    const users = session_user.username;

    Issue.getUserByuser(users, function (err, user) {
        if (err) throw err;

        res.render('merchant/issuedCards', {items: user})

    });
});

router.get('/pending_cards', function(req, res, next) {
    const session_user = res.locals.user;
    const users = session_user.username;
    Card_req.getUserByUsername(users, function (err, user) {
        if (err) throw err;
        res.render('merchant/requestcard', {items: user})


    });
});

router.post('/pending_cards', function (req, res) {
    const submit = req.body.uusername;
    var id = req.body.id;
    Card_req.updateUserById(id, function(err, user){
    if (err) throw err;
    console.log(user);
    var data = {
    username : user.uusername,
    amount : user.amount
    }
    Issue.updateAountByUsername(data, function(err, user){
                console.log(user);
            })


/*
    Card_req.getUserByUsername(submit, function (err, user) {
        if (err) throw err;
        user.forEach(function(value){
        var details ={
        username: value.uusername,
        amount: value.amount,
            }
            Issue.getUsersByUsername(user.username, function(err, user){
                console.log(user);
            })
        })
        res.render('merchant/profile', {items: user})
    })*/
    })
});




module.exports = router;