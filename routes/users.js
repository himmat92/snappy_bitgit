var express = require('express');
var router = express.Router();
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var objectId = require('mongodb').ObjectID;

var Merchant = require('../models/merchant');
var User = require('../models/user');
var Issue = require('../models/issueCards');
var Card_req = require('../models/cardrequest');

function ensureAuthenticated(req, res, next){
    if(req.isAuthenticated()){
        return next();
    } else {
        //req.flash('error_msg','You are not logged in');
        res.redirect('/users/login');
    }
}



// Register
router.get('/register', function (req, res) {
	res.render('user/register');
});

router.get('/profile', ensureAuthenticated, function (req, res) {
    res.render('user/profile');
});


router.get('/issuedcards', ensureAuthenticated, function(req, res){
    res.render('user/cards-inventory');
});

router.get('/reedem_card', ensureAuthenticated, function(req, res){
    res.render('user/redeem');
});

router.get('/request_card', ensureAuthenticated, function(req, res){
	res.render('user/requestcard')
});

// Login
router.get('/login', function (req, res) {
	res.render('user/login');
});

// Register User
router.post('/register', function (req, res) {
	var name = req.body.name;
	var email = req.body.email;
	var username = req.body.username;
	var password = req.body.password;
	var password2 = req.body.password2;

	// Validation
	req.checkBody('name', 'Name is required').notEmpty();
	req.checkBody('email', 'Email is required').notEmpty();
	req.checkBody('email', 'Email is not valid').isEmail();
	req.checkBody('username', 'Username is required').notEmpty();
	req.checkBody('password', 'Password is required').notEmpty();
	req.checkBody('password2', 'Passwords do not match').equals(req.body.password);

	var errors = req.validationErrors();

	if (errors) {
		res.render('register', {
			errors: errors
		});
	}
	else {
		//checking for email and username are already taken
		User.findOne({ username: { 
			"$regex": "^" + username + "\\b", "$options": "i"
	}}, function (err, user) {
			User.findOne({ email: { 
				"$regex": "^" + email + "\\b", "$options": "i"
		}}, function (err, mail) {
				if (user || mail) {
					res.render('user/register', {
						user: user,
						mail: mail
					});
				}
				else {
					var newUser = new User({
						name: name,
						email: email,
						username: username,
						password: password
					});
					User.createUser(newUser, function (err, user) {
						if (err) throw err;
					});
         	req.flash('success_msg', 'You are registered and can now login');
					res.redirect('/users/login');
				}
			});
		});
	}
});


router.post('/login',
	passport.authenticate('user', { successRedirect: '/users/profile', failureRedirect: '/users/login', failureFlash: true }),
	function (req, res) {
		res.redirect('/');
	});

router.get('/logout', function (req, res) {
	req.logout();

	req.flash('success_msg', 'You are logged out');

	res.redirect('/users/login');
});

router.post('/purchase', function (req, res) {
    var amount = req.body.amount;

    // Validation
    req.checkBody('amount', 'This is required').notEmpty();

    var errors = req.validationErrors();

    if (errors) {
        res.render('/user/profile', {
            /*errors: errors*/
        });
    }
    else {
        var newPurchase = new Purchase({
            amount: amount,

        });
        Purchase.createPurchase(newPurchase, function (err, amount) {
            if (err) throw err;

        });
        req.flash('success_msg', 'You have added a new gift card');
        res.redirect('/users/profile');
    }
});

router.get('/get-dataa', function(req, res, next) {
    const session_user = res.locals.user;
    const username = session_user.username;
    // pushing the data into the model from the beneath function
    Issue.getUserByUsername(username, function (err, user) {
        if (err) throw err;
        var remaining_balance = 0 ;
        user.forEach(function(value){
            remaining_balance += parseInt(value.remaining_balance);
        });
        res.render('user/cards-inventory', {items: user, remaining_balance})

    });
});

router.post('/redeem_card', function (req, res) {
    const submit = req.body.username;
    var id = req.body.id;
    Issue.updateUserById(objectId(id), function (err, user) {
        if (err) throw err;
        res.render('user/profile', {items: user})

    })
});

router.post('/manual_redeem', function (req, res) {
    var uniqid = req.body.uniqid;
    var name = req.body.name;
    var amount = req.body.amount;

    Issue.getUsersByUniqid(uniqid, function (err, user) {
        if (err) throw err;
        var id = user._id;
        /*        var amount2 = user.amount; */
        var rem_bal = user.remaining_balance;
        var new_rem_bal = parseInt(rem_bal) - parseInt(amount);
        var data = {
            id: id,
            rem_bal: new_rem_bal,
        }
        Issue.updateUsersById(data, function (err, user) {
            if (err) throw err;
            res.render('user/profile')

        })

    })
});

router.post('/request_card', function (req, res) {
    const session_user = res.locals.user;
    const uusername = session_user.username;

    var username = req.body.name;
    var email = req.body.email;
    var amount = req.body.amount;

    // Validation
    req.checkBody('name', 'Name is required').notEmpty();
    req.checkBody('email', 'Email is required').notEmpty();
    req.checkBody('email', 'Email is not valid').isEmail();
    req.checkBody('amount', 'Amount is required').notEmpty();

    var errors = req.validationErrors();

    if (errors) {
        res.render('register', {
            errors: errors
        });
    }
    else {
        //checking for email and username are already taken
        Merchant.findOne({
            username: {
                "$regex": "^" + username + "\\b", "$options": "i"
            }
        }, function (err, user) {
            if (!user) {
                res.render('register');
            }
            else {
                var item = {
                    username: username,
                    email: email,
                    amount: amount,
                    status: "pending",
                    uusername: uusername,
                };
                var req_card = new Card_req(item);
                req_card.save();

                req.flash('success_msg', 'You have requested for gift card');
                res.redirect('profile');
            }

        })
    }
});


module.exports = router;